package com.custard130.scripts.sminer.mine;

import org.powerbot.script.Tile;
import org.powerbot.script.rt6.ClientContext;

public class VarrockWest extends Mine {

	public VarrockWest(ClientContext arg0) {
		super(arg0);
		setBankPath(new Tile[] { new Tile(3171, 3365, 0),
				new Tile(3170, 3372, 0), new Tile(3170, 3381, 0),
				new Tile(3171, 3396, 0), new Tile(3170, 3408, 0),
				new Tile(3171, 3420, 0), new Tile(3177, 3429, 0),
				new Tile(3185, 3432, 0) });
		setMinePath(new Tile[] { new Tile(3185, 3436, 0),
				new Tile(3185, 3433, 0), new Tile(3177, 3430, 0),
				new Tile(3172, 3419, 0), new Tile(3168, 3409, 0),
				new Tile(3171, 3400, 0), new Tile(3171, 3390, 0),
				new Tile(3171, 3381, 0), new Tile(3171, 3371, 0),
				new Tile(3174, 3366, 0) });

		rocks.add("tin");
		rocks.add("clay");
		rocks.add("iron");

		//init();
	}

}
