package com.custard130.scripts.sminer.mine;

import org.powerbot.script.Tile;
import org.powerbot.script.rt6.ClientContext;

public class VarrockEast extends Mine {

	public VarrockEast(ClientContext arg0) {
		super(arg0);

		setBankPath(new Tile[] { new Tile(3283, 3368, 0),
				new Tile(3290, 3377, 0), new Tile(3292, 3386, 0),
				new Tile(3292, 3395, 0), new Tile(3292, 3403, 0),
				new Tile(3285, 3413, 0), new Tile(3281, 3422, 0),
				new Tile(3273, 3428, 0), new Tile(3265, 3428, 0),
				new Tile(3258, 3428, 0), new Tile(3253, 3422, 0) });
		setMinePath(new Tile[] { new Tile(3253, 3422, 0),
				new Tile(3259, 3428, 0), new Tile(3273, 3428, 0),
				new Tile(3280, 3423, 0), new Tile(3285, 3412, 0),
				new Tile(3291, 3400, 0), new Tile(3291, 3388, 0),
				new Tile(3293, 3380, 0), new Tile(3289, 3373, 0),
				new Tile(3286, 3370, 0), new Tile(3286, 3365, 0) });
		rocks.add("tin");
		rocks.add("copper");
		rocks.add("iron");

		//init();
	}

}
