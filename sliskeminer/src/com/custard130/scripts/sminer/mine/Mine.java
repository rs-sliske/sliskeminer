package com.custard130.scripts.sminer.mine;

import java.util.ArrayList;

import org.powerbot.script.Tile;
import org.powerbot.script.rt6.ClientAccessor;
import org.powerbot.script.rt6.ClientContext;
import org.powerbot.script.rt6.TilePath;

public class Mine extends ClientAccessor{

	public Mine(ClientContext arg0) {
		super(arg0);		
	}
	//protected Tile[] tilesToBank;
	protected TilePath pathToBank;
	//protected Tile[] tilesToMine;
	protected TilePath pathToMine;
	
	protected ArrayList<String> rocks=new ArrayList<String>();
	
	private String[] availableRocks;
	
	protected void setBankPath(Tile[] tiles){
		//tilesToBank = tiles;
		pathToBank = ctx.movement.newTilePath(tiles);
	}
	protected void setMinePath(Tile[] tiles){
		//tilesToMine = tiles;
		pathToMine = ctx.movement.newTilePath(tiles);
	}
	
	protected void init(){
//		pathToBank = ctx.movement.newTilePath(tilesToBank);
//		pathToMine = ctx.movement.newTilePath(tilesToMine);
	}
	
	public boolean walkToBank (){
		return pathToBank.traverse();
	}
	public boolean walkToMine(){
		return pathToMine.traverse();
	}
	public String[] getRocks(){
		return availableRocks;
	}
}
