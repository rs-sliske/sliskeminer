package com.custard130.scripts.sminer;

import static com.custard130.scripts.sminer.util.Methods.walkToObject;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.event.MenuEvent;

import org.powerbot.script.BotMenuListener;
import org.powerbot.script.Condition;
import org.powerbot.script.MessageEvent;
import org.powerbot.script.MessageListener;
import org.powerbot.script.PaintListener;
import org.powerbot.script.PollingScript;
import org.powerbot.script.Script;
import org.powerbot.script.Tile;
import org.powerbot.script.rt6.GameObject;
import org.powerbot.script.rt6.Player;

import com.custard130.scripts.sminer.mine.Mine;
import com.custard130.scripts.sminer.mine.VarrockEast;
import com.custard130.scripts.sminer.mine.VarrockWest;
import com.custard130.scripts.sminer.rocks.Clay;
import com.custard130.scripts.sminer.rocks.Copper;
import com.custard130.scripts.sminer.rocks.Gems;
import com.custard130.scripts.sminer.rocks.Iron;
import com.custard130.scripts.sminer.rocks.Rock;
import com.custard130.scripts.sminer.rocks.Tin;
import com.custard130.scripts.sminer.util.Timer;
import com.custard130.scripts.sminer.util.Tracker;
import com.custard130.scripts.sminer.wrappers.ClientContext;

@Script.Manifest(
		name = "Sliskes Varrock Miner",
		description = "Mines rocks while you fap.")
public class SliskeMiner extends PollingScript<ClientContext> implements
		PaintListener, MessageListener, BotMenuListener {

	enum State {
		MINING,
		WALKING_TO_BANK,
		WALKING_TO_MINE,
		DROPPING,
		BANKING,
		LOADING,
		STOPPING,
		LOST
	};

	private static boolean	banking		= true;
	private Options			window;
	private Tile			startTile;
	private State			currentState;

	private static String	currentRock	= "iron";
	private static String	currentMine	= "varrock west";

	private Tracker			stats		= new Tracker(ctx);

	private Gems			gems;

	private Player player() {
		return ctx.players.local();
	}

	private HashMap<String, Rock>	rocks		= new HashMap<String, Rock>();
	private HashMap<String, Mine>	mines		= new HashMap<String, Mine>();
	private ArrayList<String>		rockMined	= new ArrayList<String>();

	public void start() {
		// stats =
		startTile = player().tile();
		rocks.put("iron", new Iron(ctx));
		rocks.put("copper", new Copper(ctx));
		rocks.put("clay", new Clay(ctx));
		rocks.put("tin", new Tin(ctx));
		mines.put("varrock west", new VarrockWest(ctx));
		mines.put("varrock east", new VarrockEast(ctx));
		gems = new Gems(ctx);
		loadGUI();
	
	}

	public void loadGUI() {
		if (window == null) {
			window = new Options(mines);
		}
		window.setVisible(true);
	}

	public static void setup(String rock, String mine, boolean bank) {
		currentRock = rock;
		currentMine = mine;
		banking = bank;
		System.out.println("settings changed");
		System.out.println(currentRock + " " + currentMine + " " + banking);
	}

	@Override
	public void poll() {
		stats.update();
		if (!rockMined.contains(currentRock)) {
			rockMined.add(currentRock);
		}
		currentState = getState();
		switch (currentState) {
			case LOADING:
				break;
			case BANKING:
				if (ctx.bank.bankAll()) {
					currentState = State.WALKING_TO_MINE;
				}
				break;
			case MINING:
				if (player().idle())
					mineRock(rocks.get(currentRock).rockIDs());
				break;
			case WALKING_TO_BANK:
				mines.get(currentMine).walkToBank();
				break;
			case DROPPING:
				drop();
				break;
			case WALKING_TO_MINE:
				mines.get(currentMine).walkToMine();
				break;
			default:
				break;

		}

	}

	private State getState() {
		if (window.isVisible())
			return State.LOADING;
		if (ctx.invent.isFull()) {
			if (banking) {
				if (ctx.bank.nearest().tile().distanceTo(player().tile()) < 10) {
					return State.BANKING;
				} else
					return State.WALKING_TO_BANK;
			} else
				return State.DROPPING;
		} else {
			if (startTile.distanceTo(player().tile()) < 10) {
				return State.MINING;
			} else
				return State.WALKING_TO_MINE;
		}
	}

	private void mineRock(final int[] ids) {
		walkToObject(ids, ctx);
		GameObject rock = ctx.objects.select().id(ids).within(5).poll();
		if (rock == null)
			return;
		if (rock.click("Mine")) {
			Condition.sleep(100);
			while (!player().idle()) {
			}
		}
	}

	private void drop() {
		String[] oreNames = (String[]) rockMined.toArray();
		ctx.invent.select().name(oreNames);
		while (ctx.invent.count() > 0) {
			ctx.invent.poll().interact("Drop");
		}
	}

	// public int mined = 0;

	@Override
	public void repaint(Graphics g) {

		g.setColor(Color.BLACK);
		g.fillRect(0, 0, 300, 100);
		g.setColor(Color.RED);
		g.drawString("Current State : " + currentState, 10, 20);
		g.drawString("        Mined : " + stats.mined, 10, 35);
		g.drawString("      Ran For : " + Timer.getTimeRan(), 10, 50);
		g.drawString("      xp/h : " + stats.getMiningXpRate(), 10, 65);
		g.drawString("   gp made : " + getMoneyMade(), 10, 80);
	}

	@Override
	public void messaged(MessageEvent e) {
		String message = e.text();
		gems.checkGems(message);
		if (message.contains("manage to mine")) {
			stats.mined++;
			for (String s : rockMined) {
				rocks.get(s).checkMSG(e, s);
			}
		}

	}

	@Override
	public void menuCanceled(MenuEvent e) {
	}

	@Override
	public void menuDeselected(MenuEvent e) {
	}

	@Override
	public void menuSelected(MenuEvent e) {
		final JMenu menu = (JMenu) e.getSource();
		final JMenuItem gui = new JMenuItem("Show GUI");
		gui.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				loadGUI();
			}
		});
		menu.add(gui);
	}

	private int getMoneyMade() {
		int res = gems.getMoney();
		for (String s : rockMined) {
			res += rocks.get(s).getMoneyMade();
		}
		return res;
	}

}
