package com.custard130.scripts.sminer;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JToggleButton;

import com.custard130.scripts.sminer.mine.Mine;

public class Options extends JFrame{
	private static final long	serialVersionUID	= 1L;

	public Options(){
		
	}
	
	public Options(HashMap<String,Mine> mines) {
		
		setResizable(false);
		setBounds(100, 100, 300, 350);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		setLocationRelativeTo(null);
		setVisible(true);
		
		final JToggleButton tglbtnBank = new JToggleButton("Bank");
		tglbtnBank.setBounds(86, 29, 121, 23);
		getContentPane().add(tglbtnBank);
		
		
		final JComboBox location = new JComboBox();
		location.setModel(new DefaultComboBoxModel(new String[] {"varrock east", "varrock west"}));
		location.setBounds(72, 112, 150, 20);
		getContentPane().add(location);
		final String curLoc = (String) location.getSelectedItem();
		
		JLabel lblLocation = new JLabel("Location");
		lblLocation.setLabelFor(location);
		lblLocation.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLocation.setBounds(120, 87, 59, 14);
		getContentPane().add(lblLocation);
		
		JLabel lblRock = new JLabel("Rock");
		lblRock.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblRock.setBounds(120, 174, 39, 14);
		getContentPane().add(lblRock);
		
		
		final JComboBox rocks = new JComboBox();
		rocks.setModel(new DefaultComboBoxModel(new String[]{"copper","tin","clay","iron"}));
		rocks.setBounds(72, 199, 150, 20);
		getContentPane().add(rocks);
		final String curRock = (String) rocks.getSelectedItem();
		
		JButton btnStart = new JButton("START");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean bank = tglbtnBank.isSelected();
				final String curLoc = (String) location.getSelectedItem();
				final String curRock = (String) rocks.getSelectedItem();
				SliskeMiner.setup(curRock, curLoc, bank);
				System.out.println(curRock+" : "+curLoc+" : "+bank);
				setVisible(false);
			}
		});
		btnStart.setBackground(Color.GREEN);
		btnStart.setBounds(103, 264, 89, 23);
		getContentPane().add(btnStart);
	}
}
