package com.custard130.scripts.sminer.wrappers;

import org.powerbot.script.rt6.Player;

import com.custard130.scripts.sminer.util.GrandExchange;

public class ClientContext extends org.powerbot.script.rt6.ClientContext {
	//public static ClientContext	current;

	public final Inventory		invent;
	public final GrandExchange	ge;
	public final Player			local;
	public final Bank			bank;

	public ClientContext(ClientContext ctx) {
		super(ctx);
		//current = this;
		this.invent = new Inventory(this);
		this.ge = new GrandExchange(this);
		this.local = players.local();
		this.bank = new Bank(this);
	}
}
