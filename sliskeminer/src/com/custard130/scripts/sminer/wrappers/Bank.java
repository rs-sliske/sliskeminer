package com.custard130.scripts.sminer.wrappers;

import org.powerbot.script.Tile;
import org.powerbot.script.rt6.ClientContext;

public class Bank extends org.powerbot.script.rt6.Bank {

	public Bank(ClientContext arg0) {
		super(arg0);

	}

	public void walkTo() {
		ctx.movement.findPath(nearestTile()).traverse();

		// ctx.movement.newTilePath(arg0)
	}

	private Tile nearestTile() {
		return nearest().tile();
	}

	public int countItem(final int id) {
		return select().id(id).count();
	}
	
	public boolean bankAll() {
		if (open()) {
			if (depositInventory()) {
				if (close())
					return true;
			}
		}
		return false;
	}

}
