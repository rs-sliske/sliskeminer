package com.custard130.scripts.sminer.wrappers;

import org.powerbot.script.rt6.*;

public class Inventory extends Backpack {

	public Inventory(ClientContext ctx) {
		super(ctx);

	}


	public boolean isFull() {
		return ctx.backpack.select().count() == 28;
	}

	public int count(int itemID) {
		return ctx.backpack.select().id(itemID).count();
	}

	public boolean contains(int itemID) {
		return count(itemID) > 0;
	}

}
