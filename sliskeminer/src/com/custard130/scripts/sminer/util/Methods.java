package com.custard130.scripts.sminer.util;

import java.util.Random;

import org.powerbot.script.rt6.GameObject;

import com.custard130.scripts.sminer.wrappers.ClientContext;

public class Methods {
	private Random	random	= new Random();

	public static int intArrayTotal(int[] args) {
		int res = 0;
		for (int i : args) {
			res += i;
		}
		return res;
	}

	public static boolean walkToObject(final int[] objectID, ClientContext ctx) {
		GameObject target = ctx.objects.select().id(objectID).nearest().poll();
		if (!target.inViewport()) {
			ctx.camera.turnTo(target);
		}
		ctx.movement.findPath(target.tile()).traverse();
		return false;
	}

	public static int weightedRandom(final int choices) {
		for (int x = choices; x > 0; x--) {
			for (int i = 100; i > 0; i--) {
				if ((i / x) % 1 == 0) {
					return x - 1;
				}
			}
		}
		return 0;

	}

}
