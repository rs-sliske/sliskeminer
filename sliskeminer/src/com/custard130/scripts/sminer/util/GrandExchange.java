package com.custard130.scripts.sminer.util;

import java.util.HashMap;

import org.powerbot.script.rt6.GeItem;

import com.custard130.scripts.sminer.wrappers.ClientContext;

public class GrandExchange {

	private static HashMap<Integer, GeItem>	items;
	private ClientContext					ctx;

	public GrandExchange(ClientContext clientContext) {
		items = new HashMap<Integer, GeItem>();
		this.ctx = clientContext;
	}

	public int getPrice(final int itemID) {
		if (!items.containsKey(itemID)) {
			items.put(itemID, GeItem.profile(itemID));
		}
		return items.get(itemID).price(GeItem.PriceType.CURRENT).price();
	}

}
