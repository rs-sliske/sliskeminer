package com.custard130.scripts.sminer.util;

public class Timer {

	enum format {
		MILI, SEC, MIN, HOUR
	}

	public static final long startTime = System.currentTimeMillis();

	public static double ranFor(format f) {
		long curTime = System.currentTimeMillis();
		double temp = curTime - startTime;
		switch (f) {
		case MILI:
			return temp;

		case SEC:
			return temp / 1000.0;
		case MIN:
			return temp / 60000.0;
		case HOUR:
			return temp / 3600000.0;
		}
		return -1;
	}

	public static String getTimeRan() {
		int temp = (int) ranFor(format.SEC);
		int hours = temp / 3600;
		int mins = temp / 60;
		int secs = temp % 60;
		StringBuilder sf = new StringBuilder();
		sf.append(hours).append(" : ");
		sf.append(mins).append(" : ");
		sf.append(secs);
		return sf.toString();
	}

}
