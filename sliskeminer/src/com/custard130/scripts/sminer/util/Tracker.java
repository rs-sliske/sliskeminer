package com.custard130.scripts.sminer.util;

import org.powerbot.script.MessageEvent;
import org.powerbot.script.MessageListener;
import org.powerbot.script.rt6.ClientContext;
import org.powerbot.script.rt6.Skills;

import com.custard130.scripts.sminer.util.Timer.format;

public class Tracker implements MessageListener {

	private ClientContext	ctx;

	private int				startXpMining;
	private int				startXpTotal;

	private int				miningXpGained;
	private int				totalXpGained;

	private int				miningXpPerHour;
	private int				totalXpPerHour;

	public int				mined		= 0;

	public Tracker(ClientContext arg0) {
		ctx = arg0;
		startXpMining = curMiningXP();
		startXpTotal = curTotalXP();
	}

	public void update() {
		miningXpGained = curMiningXP() - startXpMining;
		totalXpGained = curTotalXP() - startXpTotal;

		miningXpPerHour = (int) ((miningXpGained / Timer.ranFor(format.MILI)) * 3600);
		totalXpPerHour = (int) ((totalXpGained / Timer.ranFor(format.MILI)) * 3600);

	}
	private int curMiningXP() {
		return ctx.skills.experience(Skills.MINING);
	}

	private int curTotalXP() {
		return Methods.intArrayTotal(ctx.skills.experiences());
	}

	public String getMiningXpRate() {
		int temp = miningXpPerHour;// 1000;
		return new String(temp + "k");
		// return "20k";
	}

	public String getTotalXpRate() {
		int temp = totalXpPerHour / 1000;
		return new String(temp + "k");
	}

	@Override
	public void messaged(MessageEvent e) {
		if (e.text().contains("manage to mine")) {
			mined++;
		}
	}

}
