package com.custard130.scripts.sminer.rocks;

import com.custard130.scripts.sminer.wrappers.ClientContext;

import java.util.HashMap;

public class Gems {
	private HashMap<String, Gem>	gems	= new HashMap<String, Gem>();
	private ClientContext ctx;

	public Gems(ClientContext ctx) {
		this.ctx=ctx;
		gems.put("sapphire", new Gem(1));
		gems.put("emerald", new Gem(1));
		gems.put("ruby", new Gem(1));
		gems.put("diamond", new Gem(1));
	}

	public int getMoney() {
		int res = 0;
		for (Gem g : gems.values()) {
			res += g.money();
		}
		return res;
	}
	
	public void checkGems(String message){
		message = message.toLowerCase();
		for(String s:gems.keySet()){
			if (message.contains(s)){
				gems.get(s).recieved();
				return;
			}
		}
	}

	private class Gem {
		private int	price;
		private int	counter;

		private Gem(int id) {
			counter = 0;
			price = ctx.ge.getPrice(id);
		}

		private void recieved() {
			counter++;
		}

		public int money() {
			return counter * price;
		}

	}
}
