package com.custard130.scripts.sminer.rocks;

import java.util.ArrayList;

import org.powerbot.script.MessageEvent;
import org.powerbot.script.MessageListener;
import org.powerbot.script.Tile;

import com.custard130.scripts.sminer.wrappers.ClientContext;

public abstract class Rock implements MessageListener {

	protected int					oreID;
	private int						price;
	protected ArrayList<Tile>		locations	= new ArrayList<Tile>();
	protected ArrayList<Integer>	rocks		= new ArrayList<Integer>();
	protected int					mined;

	protected Rock(int id,ClientContext ctx) {
		oreID = id;
		price = ctx.ge.getPrice(oreID);
		mined = 0;
		addLocs();
		addRockIDs();
	}

	protected abstract void addLocs();

	protected abstract void addRockIDs();

	@SuppressWarnings("unused")
	private Tile nearestRock(ClientContext ctx) {
		double closestDist = -1;
		Tile closest = ctx.local.tile();
		for (Tile t : locations) {
			double tempDist = ctx.local.tile().distanceTo(t);
			if ((closestDist < 0) || (tempDist < closestDist)) {
				closestDist = tempDist;
				closest = t;
			}
		}
		return closest;
	}

	public int getMoneyMade() {
		return mined * price;
	}

	public int[] rockIDs() {
		int[] temp = new int[rocks.size()];
		int i = 0;
		for (Integer r : rocks) {
			temp[i++] = r;
		}
		return temp;
	}

	public int oreID() {
		return oreID;
	}

	protected void addLoc(Tile loc) {
		locations.add(loc);
	}

	protected void addRock(int id) {
		rocks.add(id);
	}

	public void checkMSG(MessageEvent e, String s) {
		if (e.text().toLowerCase().contains(s)) {
			mined++;
			// System.out.println(e.text());
		}
	}
}
