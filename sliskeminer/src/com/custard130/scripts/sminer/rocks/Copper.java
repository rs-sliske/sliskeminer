package com.custard130.scripts.sminer.rocks;

import org.powerbot.script.MessageEvent;

import com.custard130.scripts.sminer.wrappers.ClientContext;

public class Copper extends Rock {

	public Copper(ClientContext ctx) {
		super(436,ctx);
		
	}

	@Override
	protected void addLocs() {
		

	}

	@Override
	protected void addRockIDs() {
		addRock(11960);
		addRock(11961);
		addRock(11962);
	}

	@Override
	public void messaged(MessageEvent e) {
		checkMSG(e,"copper");
	}

}
