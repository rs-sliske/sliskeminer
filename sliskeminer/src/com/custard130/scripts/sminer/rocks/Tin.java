package com.custard130.scripts.sminer.rocks;

import org.powerbot.script.MessageEvent;

import com.custard130.scripts.sminer.wrappers.ClientContext;

public class Tin extends Rock {

	public Tin(ClientContext ctx) {
		super(438,ctx);
		
	}

	@Override
	protected void addLocs() {
		

	}

	@Override
	protected void addRockIDs() {
		addRock(11957);
		addRock(11958);
		addRock(11959);
	}

	@Override
	public void messaged(MessageEvent e) {
		checkMSG(e,"tin");
	}

}
