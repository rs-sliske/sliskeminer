package com.custard130.scripts.sminer.rocks;

import org.powerbot.script.MessageEvent;

import com.custard130.scripts.sminer.wrappers.ClientContext;


public class Iron extends Rock{
	public Iron(ClientContext ctx){
		super(440,ctx);
		
	}
	protected void addLocs(){
		
	}
	protected void addRockIDs(){
		addRock(11954);
		addRock(11955);
		addRock(11956);
	}
	@Override
	public void messaged(MessageEvent e) {
		checkMSG(e,"iron");
	}
}
