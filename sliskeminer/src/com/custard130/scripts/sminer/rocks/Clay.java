package com.custard130.scripts.sminer.rocks;

import org.powerbot.script.MessageEvent;

import com.custard130.scripts.sminer.wrappers.ClientContext;

public class Clay extends Rock{

	public Clay(ClientContext ctx) {
		super(434,ctx);
		
	}

	@Override
	protected void addLocs() {
		
		
	}

	@Override
	protected void addRockIDs() {
		addRock(15503);
		addRock(15504);
		addRock(15505);
		
		
	}

	@Override
	public void messaged(MessageEvent e) {
		checkMSG(e,"clay");
	}

}
